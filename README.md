# exercise-2.1-remove-dups-part-b

Write code to remove duplicates from an unsorted linked list.

FOLLOW UP

How would you solve this problem if a temporary buffer is not allowed?

_Hints: #9, #40_

Provide a solution that does not use additional storage, i.e. works in constant space at the expense of additional time complexity O(n^2).
