data class Node(var value: Int, var next: Node? = null)

fun createLinkedList(vararg values: Int): Node? {
    fun appendToList(head: Node) {
        var current = head
        fun addNodeToList(node: Node) {
            current.next = node
            current = node
        }

        for (n in 1 until values.size) addNodeToList(Node(values[n]))
    }
    val head = if (values.isNotEmpty()) Node(values[0]) else null

    if (head != null && values.size > 1) appendToList(head)
    return head
}

fun removeDups(head: Node?) {
    tailrec fun examineAll(base: Node?) {
        tailrec fun testAndExamineRest(base: Node, prev: Node, rest: Node?) {
            fun removeDup(node: Node) {
                var next = node.next
                while (next != null && next.value == base.value) next = next.next
                if (next == null) prev.next = null else {
                    node.value = next.value
                    node.next = next.next
                }
            }

            if (rest == null) return
            if (base.value == rest.value) removeDup(rest)
            testAndExamineRest(base, rest, rest.next)
        }

        if (base == null) return
        testAndExamineRest(base, base, base.next)
        examineAll(base.next)
    }

    examineAll(head)
}
