import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `Given a single node verify no changes`() {
        val expected = Node(1, null)
        val actual = Node(1, null)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with two different nodes verify no changes`() {
        val expected = createLinkedList(1, 2)
        val actual = createLinkedList(1, 2)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with two identical nodes verify changes`() {
        val expected = createLinkedList(1)
        val actual = createLinkedList(1, 1)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with more than two nodes and only two identical nodes verify changes`() {
        val expected = createLinkedList(1, 2)
        val actual = createLinkedList(1, 2, 1)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with multiple sets of dups verify all are removed`() {
        val expected = createLinkedList(1, 2, 3)
        val actual = createLinkedList(1, 2, 1, 2, 3, 3)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with more than two dups verify all are removed`() {
        val expected = createLinkedList(1, 2, 3, 4, 5)
        val actual = createLinkedList(1, 2, 1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 4)
        removeDups(actual)
        assertEquals(expected, actual)
    }

    @Test fun `Given a linked list with dups at beginning and end verify all are removed`() {
        val expected = createLinkedList(1, 2, 3, 4, 5)
        val actual = createLinkedList(1, 1, 1, 2, 3, 3, 4, 5, 5, 5, 5)
        removeDups(actual)
        assertEquals(expected, actual)
    }
}
